/**
 * Created by Luis Gustavo on 20/06/2017.
 */

let btnInciar = document.getElementById("btnIniciar");
var score = document.getElementById("score");

btnInciar.addEventListener("click", function (event) {
    event.preventDefault();
    btnInciar.hidden = true;
    init();
});


function init(){
    configsIniciais();

    canv = document.getElementById("canvas")
    ctx = canv.getContext("2d");

    document.addEventListener("keydown", pressionarTeclado);
    setInterval(jogoSnake, 75);
}

function configsIniciais(){
    px = py = 10;
    gs = tc = 20;
    ax = ay = 15;
    xv = yv = 0;
    trail = [];
    cobra = 5;
    score.value = 0;
    actualFlow = null;
}

function jogoSnake(){
    px += xv
    py += yv;

    if(px < 0){
        px= tc-1;
    }
    if(px > tc - 1){
        px= 0;
    }
    if(py < 0){
        py= tc-1;
    }
    if(py > tc-1){
        py = 0;
    }

    ctx.fillStyle="black";
    ctx.fillRect(0, 0, canv.width, canv.height);

    ctx.fillStyle="lime";
    for(var i=0; i < trail.length; i++){
        ctx.fillRect(trail[i].x*gs, trail[i].y*gs, gs-2, gs-2);

        if(trail[i].x==px && trail[i].y==py){
            configsIniciais();
        }
    }

    //if(px == 0 || px == canv.width - gs) configsIniciais();
    //if(py == 0 || py >= (canv.height - gs)) configsIniciais();

    trail.push({x:px, y:py});
    while(trail.length > cobra){
        trail.shift();
    }

    if(ax==px && ay==py){
        cobra++;
        score.value++;
        ax = Math.floor(Math.random()*tc);
        ay = Math.floor(Math.random()*tc);
    }


    ctx.fillStyle="red";
    ctx.fillRect(ax*gs, ay*gs, gs-2, gs-2);
}

var actualFlow = null;
var d1 = 'a', d2 = 'b', d3 = 'c', d4 = 'd';

function pressionarTeclado(event){
    switch(event.keyCode){
        case 37:
            if(actualFlow != d3){
                actualFlow = d1;
                xv = -1;
                yv = 0;
            }
            break;

        case 38:
            if(actualFlow != d4){
                actualFlow = d2;
                xv = 0;
                yv = -1;
            }
            break;

        case 39:
            if(actualFlow != d1){
                actualFlow = d3;
                xv = 1;
                yv = 0;
            }
            break;

        case 40:
            if(actualFlow != d2){
                actualFlow = d4;
                xv = 0;
                yv = 1;
            }
            break;
    }
}