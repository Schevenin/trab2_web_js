/**
 * Created by Luis.Silva on 21/06/2017.
 */

const erroTelefone = "Número de Telefone Incorreto. Permitido apenas números e limite de 10 dígitos.";
const erroCelular = "Número de Celular Incorreto. Permitido apenas números e limite de 11 dígitos.";
const msgSucesso = "Formulário de serviço foi enviado com sucesso. Em breve entraremos em contato.";

let btn = document.querySelector("#btnFormulario");

btn.addEventListener("click", function (event) {
    event.preventDefault();

    let telRes = document.querySelector("#telResidencial");
    let telCel = document.querySelector("#telCelular");
    let msgModal = document.querySelector("#mensagemModal");
    let formulario = document.querySelector(".form-horizontal");

    if (validadorTelefone(msgModal, telRes, telCel)) {
        formulario.reset();
    }
});

function validadorTelefone(msgModal, numTel, numCel) {

    let modalHeader = document.querySelector(".modal-header");
    msgModal.textContent = "";
    let status = false;

    if (!/^\d+$/.test(numTel.value) || numTel.length < 10 || numTel.length > 10) {
        modalHeader.style.backgroundColor = "red";
        msgModal.textContent = erroTelefone;
        numTel.focus();
        status = false;
    }

    if (!/^\d+$/.test(numCel.value) || numCel.length < 11 || numCel.length > 11) {
        modalHeader.style.backgroundColor = "red";
        msgModal.textContent = msgModal.innerHTML + erroCelular;
        numCel.focus();
        status = false;
    }

    else {
        modalHeader.style.backgroundColor = "blue";
        msgModal.textContent = msgSucesso;
        status = true;
    }
    return status;
}